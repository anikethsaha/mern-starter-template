

![LOGO](https://imgur.com/yr0sfsR.png)
 
## MERN Starter Template

*This project is part of my final year project **MERN Stack Site Generator***


This is boilerplate for MERN stack with integrations like Redux and SSR


[![Edit MERN](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/mern-gq5eb?fontsize=14)
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://evening-woodland-07645.herokuapp.com/#/)

## Table of Contents

-   [Getting Started](#getting-started)
-   [Configurations](#configs-for-addons-v1)
    -   [`mern.config.js`](#mernjson)
-   [Starting development setup](#starting-development-setup)
    -   [Using CWA](#you-can-also-add-this-using-this-package)
-   [Start Editing](#start-editing)
-   [technology](#technology)
-   [Features](#features)
-   [license](#license)

## Getting Started

-   **Clone the project**

-   **Install the Dependencies**

```bash
$ npm i
```

-   **Run the server**

```bash
$ npm run server:dev
```

## configs for addons v1

You can add addons for this project by simply editing the `mern.json` and then running `npm run develop`

### `mern.json`

-   default options



```js
modules.exports = {
   plugins: [
        require('redux-mern-plugin')
   ]
}
```


**Run `npm run develop` or `yarn develop` after changing `mern.config.js`**


## Starting development setup

-   **Run the webpack server**

```bash
$ npm run watch
```

and, start making changes in `src/client` and `src/server` folders.

-   **For running the production server**

```bash
$ npm run server:prod
```

## Start Editing

Look for the `config/index.js` for making changes in the configs of the project

-   _Edit_ the src/client/index.js to make changes for client

-   _Edit_ the src/server/index.js to make changes in the server

## technology

-   NodeJS - Server
-   ExpressJS - Nodejs framework
-   MongoDB - Database
-   ReactJS - Frontend
-   Redux - State Management
-   Docker - Containerization and image
-   Eslint - Linter
-   Webpack - Bundler
-   Babel - Loader and Compiler/Transpiler
-   Jest - Testing Framework

## Features

-   [x] Server Side Rendering
-   [x] Image for Container
-   [x] Routing
-   [x] Model Controller Project Structure
-   [x] Build Pipelines
-   [x] Tests
-   [x] Examples (live in codesandbox)



## [LICENSE](https://github.com/anikethsaha/MERN-Boilerplate/blob/master/LICENSE)
